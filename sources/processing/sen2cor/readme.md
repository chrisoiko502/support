Sentinel-2 L2A generator with sen2cor 2.5.5
==
This docker takes as input a directory where unzipped S2 L1C files are available and
generates for each of them the corresponding L2A level with sen2cor 2.5.5.
  
Usage
==
Simply run the script `run_sen2cor.sh` to generate L2A products.  
You may have to edit the script to update the parameter `mntpath`
which must point to a valid directory where:
  - a sub-directory named "INPUT" contains unzipped L1C products
  - a sub-directory named OUTPUT where L2A will be generated

>If mntpath=/tmp, the L1C files must be in /tmp/INPUT and generated
L2A will be available in /tmp/OUTPUT

sobloo team,
