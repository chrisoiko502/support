import requests
import urllib3
from urllib3.exceptions import InsecureRequestWarning
from urllib.parse import urljoin


def get_product(p_uid, p_config):
    urllib3.disable_warnings(InsecureRequestWarning)
    url = p_config.url
    headers = p_config.headers
    cmd = urljoin(url, "/api/v1/services/download/{iid}".format(iid=p_uid))
    print("start download")
    result = requests.get(cmd, headers=headers, verify=False)
    print("end download")
    # Throw an error for bad status codes
    result.raise_for_status()
    return result
