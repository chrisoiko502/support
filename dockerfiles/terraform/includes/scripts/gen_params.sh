#set -x
source ${PWD}/setenv.sh
confdir=${terraconf}

#update tenant name in OS creds
cp ${PWD}/creds.template ${PWD}/creds
sed -i  "s/__DOMAIN_NAME__/${tenant_name}/" ${PWD}/creds
sed -i  "s/__USER_NAME__/${username}/" ${PWD}/creds

cp ${confdir}/parameters.tfvars.template ${confdir}/parameters.tfvars

sed -i  "s/__tenant_id__/${tenant_id}/" ${confdir}/parameters.tfvars
sed -i  "s/__domain_id__/${domain_id}/" ${confdir}/parameters.tfvars
sed -i  "s/__username__/${username}/" ${confdir}/parameters.tfvars
sed -i  "s/__password__/${password}/" ${confdir}/parameters.tfvars
sed -i  "s/__region__/${region}/" ${confdir}/parameters.tfvars
sed -i  "s/__nbinstances__/${nbinstances}/" ${confdir}/parameters.tfvars
sed -i  "s/__image_id__/${image_id}/" ${confdir}/parameters.tfvars
sed -i  "s/__key_name__/${key_name}/" ${confdir}/parameters.tfvars
sed -i  "s/__flavor_name__/${flavor_name}/" ${confdir}/parameters.tfvars
sed -i  "s/__sysvol_size__/${sysvol_size}/" ${confdir}/parameters.tfvars
if [ "${datavol_size}" == "0" ] ; then
  sed -i  "s/datavol_size/#datavol_size/" ${confdir}/parameters.tfvars
  sed -i  "s/datavol_type/#datavol_type/" ${confdir}/parameters.tfvars
elif [ "${datavol_type}" == "SATA" ] ; then
    sed -i  "s/datavol_type/#datavol_type/" ${confdir}/parameters.tfvars
else
  sed -i  "s/__vol_size__/${datavol_size}/" ${confdir}/parameters.tfvars
  sed -i  "s/__vol_type__/${datavol_type}/" ${confdir}/parameters.tfvars
fi
sed -i  "s#__subnet_cidr__#${subnet_cidr}#" ${confdir}/parameters.tfvars
sed -i  "s/__registry_user__/${registry_user}/" ${confdir}/parameters.tfvars
sed -i  "s/__registry_pwd__/${registry_pwd}/" ${confdir}/parameters.tfvars
sed -i  "s/__instance_name__/${instance_name}/" ${confdir}/parameters.tfvars
sed -i  "s/__availability_zone__/${availability_zone}/" ${confdir}/parameters.tfvars
sed -i  "s/__security_group_name__/${security_group_name}/" ${confdir}/parameters.tfvars
sed -i  "s/__vpc_name__/${vpc_name}/" ${confdir}/parameters.tfvars
sed -i  "s/__network_name__/${network_name}/" ${confdir}/parameters.tfvars
sed -i  "s/__subnet_name__/${subnet_name}/" ${confdir}/parameters.tfvars

