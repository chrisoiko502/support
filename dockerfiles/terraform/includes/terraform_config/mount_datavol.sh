#!/bin/sh

mkfs.ext4 /dev/vdb

mkdir /data
mount /dev/vdb /data

echo "/dev/vdb /data ext4 errors=remount-ro 0 2" >> /etc/fstab
