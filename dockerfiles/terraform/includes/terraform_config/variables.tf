### Flexible Engine Credentials
variable "username" {}

variable "password" {}

variable "domain_id" {}

variable "tenant_id" {} 

variable "endpoint" {}

variable "region" {}

variable "instance_name" {}

variable "instance_count" {}

variable "availability_zone" {}

variable "key_name" {}

variable "flavor_name" {}

variable "sysvol_size" {}

variable "image_id" {}

variable "datavol_size" {}

variable "datavol_type" {}

variable "security_group_name" {}

variable "vpc_name" {}

variable "network_name" {}

variable "subnet_name" {}

variable "subnet_cidr" {}

#variable "registry_user" {}

#variable "registry_pwd" {}

variable "dns_list" { 
  type    = "list" 
  default = [ "100.125.0.41", "100.126.0.41" ]
}
