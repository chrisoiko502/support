#!/bin/bash
if [ -z $bucket ] || [ -z $id ] || [ -z $key ] ; then
  echo "Usage: [bucket_name] [s3_ID] [s3_key]"
else
  mkdir -p /share/s3
  chmod 777 /share/s3
  # Create password file with provided arguments
  echo "$id:$key" > /etc/passwd-s3fs
  chmod 600 /etc/passwd-s3fs
  # mount s3 storage
  s3fs $bucket /share/s3 -o passwd_file=/etc/passwd-s3fs -o url="http://oss.eu-west-0.prod-cloud-ocb.orange-business.com/"
fi
